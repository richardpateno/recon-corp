let navItems = document.querySelector("#navSession");

// localStorage => an object used to store information indefinitely locally in our devices
let userToken = localStorage.getItem("token");
let adminStatus = localStorage.getItem("isAdmin");
/*
localStorage {
	token: "23o8ru32hrlh23kjlrhk2j3"
	isAdmin: false,
	getItem: function()
	setItem: function()
}
*/

// `` = backticks/julius back ticks/template string

//conditional rendering:
//dynamically add or delete an html element based on a condition
//Here we are adding our login and register links in the navbar ONLY if the user is not logged in.
//Otherwise, only the log out link will be shown.
//innerHTML = contains all of the element's childen as a string.
console.log(window.location.pathname);
if (!userToken) {
  if (window.location.pathname.includes("login")) {
    navItems.innerHTML += `
		<li class="nav-item active">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item ">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

	`;
  } else if (window.location.pathname.includes("register")) {
    navItems.innerHTML += `
		<li class="nav-item ">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item active">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

	`;
  } else {
    navItems.innerHTML += `
		<li class="nav-item ">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item ">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

	`;
  }
} else {
  if (adminStatus === "true") {
    navItems.innerHTML += `

		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>

		`;
  } else {
    navItems.innerHTML += `

		<li class="nav-item ">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
		<li class="nav-item ">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>

		`;
  }
}
